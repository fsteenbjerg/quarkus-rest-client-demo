package dk.stonemountain.demo.quarkus.restclient.oauth;

import javax.json.bind.annotation.JsonbProperty;

public class TokensDTO {
	@JsonbProperty("access_token")
	private String accessToken;

	@JsonbProperty("token_type")
	private String tokenType;

	@JsonbProperty("expires_in")
	private long expiresIn;

	@JsonbProperty("refresh_expires_in")
	private long refreshExpiresIn;

	@JsonbProperty("refresh_token")
	private String refreshToken;

	@JsonbProperty("not-before-policy")
	private int notBeforePolicy;

	@JsonbProperty("session_state")
	private String sessionState;

	@JsonbProperty("scope")
	private String scope;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(long expiresIn) {
		this.expiresIn = expiresIn;
	}

	public long getRefreshExpiresIn() {
		return refreshExpiresIn;
	}

	public void setRefreshExpiresIn(long refreshExpiresIn) {
		this.refreshExpiresIn = refreshExpiresIn;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public int getNotBeforePolicy() {
		return notBeforePolicy;
	}

	public void setNotBeforePolicy(int notBeforePolicy) {
		this.notBeforePolicy = notBeforePolicy;
	}

	public String getSessionState() {
		return sessionState;
	}

	public void setSessionState(String sessionState) {
		this.sessionState = sessionState;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	@Override
	public String toString() {
		return "AccessTokenDTO [accessToken=" + accessToken + ", tokenType=" + tokenType + ", expiresIn=" + expiresIn
				+ ", refreshExpiresIn=" + refreshExpiresIn + ", refreshToken=" + refreshToken + ", notBeforePolicy="
				+ notBeforePolicy + ", sessionState=" + sessionState + ", scope=" + scope + "]";
	}
}