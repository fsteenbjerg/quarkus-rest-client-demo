package dk.stonemountain.demo.quarkus.restclient.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class RequestLogger implements ClientRequestFilter, ClientResponseFilter, WriterInterceptor {
	private static final Logger logger = LoggerFactory.getLogger(RequestLogger.class);
	
	private static final String ENTITY_STREAM_PROPERTY = "EntityLoggingFilter.entityStream";
	private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
	private static final int MAX_ENTITY_SIZE = 1024 * 8;
	
	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
		logger.debug("Invoking URI '{}' (method {}) and headers {} with entity : {}", requestContext.getUri(), requestContext.getMethod(), requestContext.getHeaders(), requestContext.getEntity());
		
		if (requestContext.hasEntity()) {
            final OutputStream stream = new LoggingStream(requestContext.getEntityStream());
            requestContext.setEntityStream(stream);
            requestContext.setProperty(ENTITY_STREAM_PROPERTY, stream);
        }
	}

	@Override
	public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
		try	{
			int status = responseContext.getStatus();
			final StringBuilder sb = new StringBuilder();
			if (responseContext.hasEntity()) {
                logger.debug("Received Content-type: {}", responseContext.getMediaType());
                if (!MediaType.APPLICATION_OCTET_STREAM_TYPE.equals(responseContext.getMediaType())) {
                    responseContext.setEntityStream(logInboundEntity(sb, responseContext.getEntityStream(), DEFAULT_CHARSET));
                }
			}
			
			if (status != 200 && status != 204) {
				logger.error("URI {} gave status {} had headers '{}' and entity '{}'", requestContext.getUri(), status, responseContext.getHeaders(), sb);
			} else {
				logger.debug("URI {} gave status {} had headers '{}' and entity '{}'", requestContext.getUri(), status, responseContext.getHeaders(), sb);
			}
		} catch (IOException e) {
			logger.error("Failure during failure handling", e);
		}
	}
	
    @Override
    public void aroundWriteTo(WriterInterceptorContext context) throws IOException {
        final LoggingStream stream = (LoggingStream) context.getProperty(ENTITY_STREAM_PROPERTY);
        context.proceed();
        if (stream != null) {
            logger.debug("Sending data: {}", stream.getStringBuilder(DEFAULT_CHARSET));
        }
    }

    private InputStream logInboundEntity(final StringBuilder b, InputStream stream, final Charset charset) throws IOException {
        if (!stream.markSupported()) {
            stream = new BufferedInputStream(stream);
        }
        stream.mark(MAX_ENTITY_SIZE + 1);
        final byte[] entity = new byte[MAX_ENTITY_SIZE + 1];
        final int entitySize = stream.read(entity);
        b.append(new String(entity, 0, Math.min(entitySize, MAX_ENTITY_SIZE), charset));
        if (entitySize > MAX_ENTITY_SIZE) {
            b.append("...more...");
        }
        b.append('\n');
        stream.reset();
        return stream;
    }
    
    private class LoggingStream extends FilterOutputStream {
        private final StringBuilder sb = new StringBuilder();
        private final ByteArrayOutputStream baos = new ByteArrayOutputStream();
 
        LoggingStream(OutputStream out) {
            super(out);
        }

        StringBuilder getStringBuilder(Charset charset) {
            // write entity to the builder
            final byte[] entity = baos.toByteArray();

            sb.append(new String(entity, 0, entity.length, charset));
            if (entity.length > MAX_ENTITY_SIZE) {
                sb.append("...more...");
            }
            sb.append('\n');

            return sb;
        }

        @Override
        public void write(final int i) throws IOException {
            if (baos.size() <= MAX_ENTITY_SIZE) {
                baos.write(i);
            }
            out.write(i);
        }
    }
}