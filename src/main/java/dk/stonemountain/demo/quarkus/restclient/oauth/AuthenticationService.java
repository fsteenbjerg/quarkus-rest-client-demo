package dk.stonemountain.demo.quarkus.restclient.oauth;

import java.time.Duration;
import java.time.LocalDateTime;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class AuthenticationService {
	private static final Logger logger = LoggerFactory.getLogger(AuthenticationService.class);
	private static final int SECONDS_BEFORE_TOKEN_EXPIRATION = 5;
	
	@Inject OAuthServerDao oauthServer;
	private TokensDTO tokens;
	private LocalDateTime timeForTokenRetrieval;

	public String getAccessToken() {
		if (tokens == null) {
			fetchAccessToken();
		}
		if (Duration.between(timeForTokenRetrieval, LocalDateTime.now()).getSeconds() > tokens.getExpiresIn() - SECONDS_BEFORE_TOKEN_EXPIRATION) {
			if (Duration.between(timeForTokenRetrieval, LocalDateTime.now()).getSeconds() > tokens.getRefreshExpiresIn() - SECONDS_BEFORE_TOKEN_EXPIRATION) {
				fetchAccessToken();
			} else {
				refreshAccessToken();
			}
		}
		return tokens.getAccessToken();
	}

	private void fetchAccessToken() {
		tokens = oauthServer.getTokens();
		logger.trace("Tokens retrieved: {}", tokens);
		timeForTokenRetrieval = LocalDateTime.now();
	}

	private void refreshAccessToken() {
		tokens = oauthServer.refreshAccessToken(tokens.getRefreshToken());
		logger.trace("Tokens retrieved: {}", tokens);
		timeForTokenRetrieval = LocalDateTime.now();
	}
}