package dk.stonemountain.demo.quarkus.restclient.oauth;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.rest.client.inject.RestClient;

@ApplicationScoped
public class OAuthServerDao {
    @Inject
    @RestClient
    AuthenticationServerResource authService;

	@PostConstruct
	public void construct() {
	}

	public TokensDTO getTokens() {
        return authService.getTokensByClientCredentials("demo", "client_credentials", "demo-universe", "50270521-dc9b-44a0-859f-48bcd481399e");
	}

	public TokensDTO refreshAccessToken(String refreshToken) {
		return authService.getTokensByRefreshToken("demo", "refresh_token", "demo-universe", "50270521-dc9b-44a0-859f-48bcd481399e", refreshToken);
	}
}