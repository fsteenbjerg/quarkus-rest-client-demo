package dk.stonemountain.demo.quarkus.restclient.oauth;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import dk.stonemountain.demo.quarkus.restclient.util.RequestLogger;

@Path("/realms")
@RegisterRestClient(configKey = "oauth-service")
public interface AuthenticationServerResource {
	@POST
	@Path("/{realm}/protocol/openid-connect/token")
	@Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
	@Produces({ MediaType.APPLICATION_JSON })
	public TokensDTO getTokensByClientCredentials(
		@PathParam("realm") String realm,
		@FormParam("grant_type") String grantType, @FormParam("client_id") String clientId,
		@FormParam("client_secret") String clientSecret);

	@POST
	@Path("/{realm}/protocol/openid-connect/token")
	@Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
	@Produces({ MediaType.APPLICATION_JSON })
	public TokensDTO getTokensByRefreshToken(
		@PathParam("realm") String realm,
		@FormParam("grant_type") String grantType, @FormParam("client_id") String clientId,
		@FormParam("client_secret") String clientSecret, @FormParam("refresh_token") String refreshToken);

}