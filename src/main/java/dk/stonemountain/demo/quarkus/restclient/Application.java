package dk.stonemountain.demo.quarkus.restclient;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.stonemountain.demo.quarkus.restclient.backend.EnvironmentResource;
import dk.stonemountain.demo.quarkus.restclient.backend.EnvironmentVariable;
import dk.stonemountain.demo.quarkus.restclient.oauth.AuthenticationService;
import io.quarkus.runtime.QuarkusApplication;

public class Application implements QuarkusApplication {
	private static final Logger log = LoggerFactory.getLogger(Application.class);
	
	@Inject
	@RestClient
	EnvironmentResource service;
	@Inject AuthenticationService authService;

	@Override
	public int run(String... args) throws Exception {
		log.debug("Args: {}", Arrays.asList(args));
		try {
			
			System.out.println("Requesting from quarkus-rest-demo ...");
			List<EnvironmentVariable> variables = service.getVariables(getAuthorizationHeader());
			System.out.println("Response received with " + variables.size() + " variables");
		} catch (Exception e) {
			log.error("Failed invoking environment service: {}", e.getMessage(), e);
			return 1;
		}
		return 0;
	}
	
	private String getAuthorizationHeader() {
		String token = authService.getAccessToken();
		return "Bearer " + token;
	}
}