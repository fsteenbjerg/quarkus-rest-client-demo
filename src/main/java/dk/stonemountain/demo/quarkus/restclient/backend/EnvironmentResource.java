package dk.stonemountain.demo.quarkus.restclient.backend;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(configKey="environment-service")
@Path("/environment")
public interface EnvironmentResource {
    @GET
    @Path("variables")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EnvironmentVariable> getVariables(@HeaderParam("Authorization") String auth);
}